<?php
namespace ITunesSearchApi\Tests\Api\Search;

use ITunesSearchApi\Api\Search\Entity;

class EntityTest extends \PHPUnit_Framework_TestCase
{
    public function testAllEntitiesAreLinkedWithMedia()
    {
        $values = Entity::values();
        foreach ($values as $value) {
            /** @var Entity $value */
            $this->assertTrue(is_string($value->getMedia()));
        }
    }
}
