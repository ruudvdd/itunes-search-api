<?php
namespace ITunesSearchApi\Tests\Api\Search;


use ITunesSearchApi\Api\Search\Entity;
use ITunesSearchApi\Api\Search\Parameters;

class ParametersTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Parameters
     */
    protected $parameters;

    protected function setUp()
    {
        $this->parameters = new Parameters('Tech45', 'US');
    }

    public function testEntityIsSetCorrectly()
    {
        $this->parameters->setEntity(Entity::MEDIA_ALL_ALBUM);
        $this->assertEquals(Entity::MEDIA_ALL_ALBUM, $this->parameters->getEntity());
    }

    public function testGetExplicitReturnsYesOrNoString()
    {
        $this->parameters->setExplicit(true);
        $this->assertEquals('Yes', $this->parameters->getExplicit());
        $this->parameters->setExplicit(false);
        $this->assertEquals('No', $this->parameters->getExplicit());
    }

    public function testMediaReturnsNullWhenEntityIsNotSet()
    {
        $this->assertEquals(null, $this->parameters->getMedia());
    }
}
