<?php
namespace ITunesSearchApi\Tests\Api\Lookup;

use ITunesSearchApi\Api\Lookup\Entity;
use ITunesSearchApi\Api\Lookup\IdName;
use ITunesSearchApi\Api\Lookup\Parameters;
use ITunesSearchApi\Api\Lookup\Sort;

class ParametersTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Parameters
     */
    protected $parameters;

    protected function setUp()
    {
        $this->parameters = new Parameters(new IdName(IdName::ISBN), 123);
    }

    public function testReturnStringWhenGettingIdName()
    {
        $this->assertTrue(is_string($this->parameters->getIdName()));
        $this->assertEquals(IdName::ISBN, $this->parameters->getIdName());
    }

    public function testIdValuesReturnCommaSeparatedListOfAllValues()
    {
        $this->parameters->addIdValues(45);
        $this->parameters->addIdValues(678);
        $this->parameters->addIdValues([9, 0]);
        $this->assertEquals('123,45,678,9,0', $this->parameters->getIdValue());
    }

    public function testSortIsSetCorrectly()
    {
        $this->parameters->setSort(Sort::RECENT);
        $this->assertEquals(Sort::RECENT, $this->parameters->getSort());
    }

    public function testEntityIsSetCorrectly()
    {
        $this->parameters->setEntity(new Entity(Entity::ALBUM));
        $this->assertEquals(Entity::ALBUM, $this->parameters->getEntity());
    }

    public function testGetSortReturnsNullWhenNotSet()
    {
        $this->assertEquals(null, $this->parameters->getSort());
    }

    public function testEntityReturnsNullIfNotSet()
    {
        $this->assertEquals(null, $this->parameters->getEntity());
    }
}
