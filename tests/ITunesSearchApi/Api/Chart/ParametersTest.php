<?php
namespace ITunesSearchApi\Tests\Api\Chart;

use ITunesSearchApi\Api\Chart\Name;
use ITunesSearchApi\Api\Chart\Parameters;

class ParametersTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Parameters
     */
    protected $parameters;

    protected function setUp()
    {
        $this->parameters = new Parameters(new Name(Name::PODCASTS));
    }

    public function testReturnStringWhenGettingIdName()
    {
        $this->assertTrue(is_string($this->parameters->getName()));
        $this->assertEquals(Name::PODCASTS, $this->parameters->getName());
    }

    /**
     * @expectedException \UnexpectedValueException
     */
    public function testIncorrectNameThrowsException()
    {
        $this->parameters->setName('Chairs');
    }
}
