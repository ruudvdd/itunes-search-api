<?php
namespace ITunesSearchApi\Tests\Http\Lookup;

use ITunesSearchApi\Api\Lookup\IdName;
use ITunesSearchApi\Api\Lookup\Parameters;
use ITunesSearchApi\Http\Lookup\ParameterQueryBuilder;

class ParameterQueryBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testBuildRemovesEmptyParameters()
    {
        $parameters = $this->getMockBuilder(Parameters::class)
            ->setConstructorArgs([IdName::ID, 123])
            ->getMock();
        $parameters
            ->method('getEntity')
            ->will($this->returnValue(null));
        $parameters
            ->method('getIdName')
            ->will($this->returnValue(IdName::ID));
        $parameters
            ->method('getIdValue')
            ->will($this->returnValue(123));

        $parameterQueryBuilder = new ParameterQueryBuilder();

        // no entity in expected value
        $this->assertEquals(IdName::ID . '=123', $parameterQueryBuilder->build($parameters));
    }
}
