<?php
namespace ITunesSearchApi\Tests\Http\Client;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use ITunesSearchApi\Http\Client\Guzzle;

class GuzzleTest extends \PHPUnit_Framework_TestCase
{
    public function testImplementedGetMethod()
    {
        $url = 'http://rvandendungen.be';
        $guzzleClient = $this->getMockBuilder(ClientInterface::class)->getMockForAbstractClass();
        $responseBody = json_encode(['success' => true]);
        $guzzleClient
            ->expects($this->once())
            ->method('request')
            ->with('get', $url)
            ->will($this->returnValue(new Response(200, [], $responseBody)));
        $guzzle = new Guzzle($guzzleClient);
        $this->assertEquals(json_decode($responseBody, true), $guzzle->get($url));
    }
}
