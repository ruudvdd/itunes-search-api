<?php
namespace ITunesSearchApi\Tests\Http\Client;

use ITunesSearchApi\Api\Chart\Name;
use ITunesSearchApi\Api\Lookup\IdName;
use ITunesSearchApi\Http\Client\AbstractClient;
use ITunesSearchApi\Http\Lookup\ParameterQueryBuilder as LookupParameterQueryBuilder;
use ITunesSearchApi\Http\Search\ParameterQueryBuilder as SearchParameterQueryBuilder;
use ITunesSearchApi\Http\Chart\ParameterQueryBuilder as ChartParameterQueryBuilder;
use ITunesSearchApi\Api\Lookup\Parameters as LookupParameters;
use ITunesSearchApi\Api\Search\Parameters as SearchParameters;
use ITunesSearchApi\Api\Chart\Parameters as ChartParameters;

class AbstractClientTest extends \PHPUnit_Framework_TestCase
{
    public function testSearchApiCall()
    {
        $parameters = $this->getMockBuilder(SearchParameters::class)
            ->setConstructorArgs(['Lookup', 'US'])
            ->getMock();
        $queryString = 'term=lookup&country=US';
        $searchParameterQueryBuilder = $this->getMockBuilder(SearchParameterQueryBuilder::class)
            ->getMock();
        $searchParameterQueryBuilder
            ->expects($this->once())
            ->method('build')
            ->with($parameters)
            ->will($this->returnValue($queryString));

        $abstractClient = $this->getMockForAbstractClass(AbstractClient::class, [[
            'search_parameter_query_builder' => $searchParameterQueryBuilder
        ]]);

        $response = ['success' => true];
        $abstractClient
            ->expects($this->once())
            ->method('get')
            ->with('https://itunes.apple.com/search?' . $queryString)
            ->will($this->returnValue($response));

        $this->assertEquals($response, $abstractClient->search($parameters));
    }

    public function testLookupApiCall()
    {
        $parameters = $this->getMockBuilder(LookupParameters::class)
            ->setConstructorArgs([IdName::ID, 123])
            ->getMock();
        $queryString = IdName::ID . '=' . 123;
        $lookupParameterQueryBuilder = $this->getMockBuilder(LookupParameterQueryBuilder::class)
            ->getMock();
        $lookupParameterQueryBuilder
            ->expects($this->once())
            ->method('build')
            ->with($parameters)
            ->will($this->returnValue($queryString));

        $abstractClient = $this->getMockForAbstractClass(AbstractClient::class, [[
            'lookup_parameter_query_builder' => $lookupParameterQueryBuilder
        ]]);

        $response = ['success' => true];
        $abstractClient
            ->expects($this->once())
            ->method('get')
            ->with('https://itunes.apple.com/lookup?' . $queryString)
            ->will($this->returnValue($response));

        $this->assertEquals($response, $abstractClient->lookup($parameters));
    }

    public function testGenresApiCall()
    {
        $abstractClient = $this->getMockForAbstractClass(AbstractClient::class);
        $genreId = 26;

        $response = ['success' => true];
        $abstractClient
            ->expects($this->once())
            ->method('get')
            ->with('https://itunes.apple.com/WebObjects/MZStoreServices.woa/ws/genres?id=' . $genreId)
            ->will($this->returnValue($response));

        $this->assertEquals($response, $abstractClient->genres($genreId));
    }

    public function testChartApiCall()
    {
        $parameters = $this->getMockBuilder(ChartParameters::class)
            ->setConstructorArgs([Name::PODCASTS])
            ->getMock();
        $queryString = 'name=' . Name::PODCASTS;
        $chartParameterQueryBuilder = $this->getMockBuilder(ChartParameterQueryBuilder::class)
            ->getMock();
        $chartParameterQueryBuilder
            ->expects($this->once())
            ->method('build')
            ->with($parameters)
            ->will($this->returnValue($queryString));

        $abstractClient = $this->getMockForAbstractClass(AbstractClient::class, [[
            'chart_parameter_query_builder' => $chartParameterQueryBuilder
        ]]);

        $response = ['success' => true];
        $abstractClient
            ->expects($this->once())
            ->method('get')
            ->with('https://itunes.apple.com/WebObjects/MZStoreServices.woa/ws/charts?' . $queryString)
            ->will($this->returnValue($response));

        $this->assertEquals($response, $abstractClient->chart($parameters));
    }

    /**
     * @expectedException ITunesSearchApi\Exception\RequestFailed
     */
    public function testHandleResponseThrowsRequestFailedOnCertainStatusCodes()
    {
        $response = 'not found';
        $statusCode = 404;
        $this->invokeAccessibleHandleResponseMethod($statusCode, $response);
    }

    public function testHandleResponseReturnsJsonDecodedResponseBodyOnCertainStatusCodes()
    {
        $response = json_encode(['success' => true]);
        $result = $this->invokeAccessibleHandleResponseMethod(200, $response);

        $this->assertEquals(json_decode($response, true), $result);
    }

    /**
     * @param $statusCode
     * @param $response
     * @return mixed
     */
    private function invokeAccessibleHandleResponseMethod($statusCode, $response)
    {
        $abstractClient = $this->getMockForAbstractClass(AbstractClient::class);
        $handleResponse = new \ReflectionMethod(AbstractClient::class, 'handleResponse');
        $handleResponse->setAccessible(true);
        return $handleResponse->invoke($abstractClient, $statusCode, $response);
    }
}
