<?php
namespace ITunesSearchApi\Tests\Http\Search;

use ITunesSearchApi\Api\Search\Parameters;
use ITunesSearchApi\Http\Search\ParameterQueryBuilder;

class ParameterQueryBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testQueryStringIsBuildCorrectly()
    {
        $term = 'Tech45 test';
        $country = 'US';
        $parameters = $this->getMockBuilder(Parameters::class)
            ->setConstructorArgs([$term, $country])
            ->getMock();
        $parameters
            ->method('getTerm')
            ->will($this->returnValue($term));
        $parameters
            ->method('getCountry')
            ->will($this->returnValue($country));
        $parameters
            ->method('getLimit')
            ->will($this->returnValue(50));

        $parameterQueryBuilder = new ParameterQueryBuilder();

        // no entity in expected value
        $this->assertEquals('term=Tech45+test&country=US&limit=50', $parameterQueryBuilder->build($parameters));
    }
}
