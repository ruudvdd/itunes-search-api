<?php

use GuzzleHttp\Client;
use ITunesSearchApi\Api\Lookup\Entity;
use ITunesSearchApi\Api\Lookup\IdName;
use ITunesSearchApi\Api\Lookup\Parameters;
use ITunesSearchApi\Http\Client\Guzzle;

include __DIR__ . '/../vendor/autoload.php';

$parameters = new Parameters(IdName::AMG_ARTIST_ID, [468749, 5723]);
$parameters
    ->setEntity(Entity::ALBUM)
    ->setLimit(5);

$client = new Guzzle(new Client());

$response = $client->lookup($parameters);

print_r($response);
