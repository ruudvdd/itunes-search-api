<?php

use GuzzleHttp\Client;
use ITunesSearchApi\Api\Chart\Name;
use ITunesSearchApi\Api\Chart\Parameters;
use ITunesSearchApi\Http\Client\Guzzle;

include __DIR__ . '/../vendor/autoload.php';

$parameters = new Parameters(Name::PODCASTS);
$parameters->setGenreId(26);
$parameters->setLimit(10);

$client = new Guzzle(new Client());

var_dump($client->chart($parameters));
