<?php
namespace ITunesSearchApi\Api\Lookup;

class Parameters
{
    /**
     * @var IdName
     */
    protected $idName;

    /**
     * @var array
     */
    protected $idValues;

    /**
     * @var Entity|null
     */
    protected $entity = null;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var Sort
     */
    protected $sort;

    /**
     * @param IdName|string $idName
     * @param string|array $idValues
     */
    public function __construct($idName, $idValues)
    {
        $this->setIdName($idName);
        $this->setIdValues($idValues);
    }

    /**
     * @return string
     */
    public function getIdName()
    {
        return $this->idName->getValue();
    }

    /**
     * @param IdName|string $idName
     * @return $this
     */
    public function setIdName($idName)
    {
        if (!($idName instanceof IdName)) {
            $idName = new IdName($idName);
        }

        $this->idName = $idName;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdValue()
    {
        return implode(',', $this->idValues);
    }

    /**
     * @param array|string $idValues
     * @return $this
     */
    public function setIdValues($idValues)
    {
        if (!is_array($idValues)) {
            $idValues = [$idValues];
        }

        $this->idValues = $idValues;
        return $this;
    }

    /**
     * @param array|string $idValues
     * @return $this
     */
    public function addIdValues($idValues)
    {
        if (!is_array($idValues)) {
            $idValues = [$idValues];
        }

        $this->idValues = array_merge($this->idValues, $idValues);
        return $this;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        if ($this->entity === null) {
            return null;
        }
        return $this->entity->getValue();
    }

    /**
     * @param Entity|string|null $entity
     * @return $this
     */
    public function setEntity($entity = null)
    {
        if (!($entity instanceof Entity) && $entity !== null) {
            $entity = new Entity($entity);
        }

        $this->entity = $entity;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        if ($this->sort === null) {
            return $this->sort;
        }
        return $this->sort->getValue();
    }

    /**
     * @param Sort|string $sort
     * @return $this
     */
    public function setSort($sort)
    {
        if (!($sort instanceof Sort)) {
            $sort = new Sort($sort);
        }

        $this->sort = $sort;
        return $this;
    }
}
