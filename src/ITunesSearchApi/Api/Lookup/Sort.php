<?php
namespace ITunesSearchApi\Api\Lookup;

use MyCLabs\Enum\Enum;

class Sort extends Enum
{
    const RECENT = 'recent';
}
