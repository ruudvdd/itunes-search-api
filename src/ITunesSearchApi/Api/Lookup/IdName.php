<?php
namespace ITunesSearchApi\Api\Lookup;

use MyCLabs\Enum\Enum;

class IdName extends Enum
{
    const ID = 'id';
    const AMG_ARTIST_ID = 'amgArtistId';
    const UPC = 'upc';
    const AMG_VIDEO_ID = 'amgVideoId';
    const ISBN = 'isbn';
}
