<?php
namespace ITunesSearchApi\Api\Lookup;

use MyCLabs\Enum\Enum;

class Entity extends Enum
{
    const ALBUM = 'album';
    const SONG = 'song';
}
