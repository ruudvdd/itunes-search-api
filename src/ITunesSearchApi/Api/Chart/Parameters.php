<?php
namespace ITunesSearchApi\Api\Chart;

class Parameters
{
    /**
     * @var Name
     */
    protected $name;

    /**
     * @var int
     */
    protected $genreId;

    /**
     * @var string country
     */
    protected $country = 'us';

    /**
     * @var int
     */
    protected $limit = 5;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->setName($name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name->getValue();
    }

    /**
     * @param string|Name $name
     * @return $this
     */
    public function setName($name)
    {
        if (!($name instanceof Name)) {
            $name = new Name($name);
        }
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getGenreId()
    {
        return $this->genreId;
    }

    /**
     * @param int $genreId
     * @return $this
     */
    public function setGenreId($genreId)
    {
        $this->genreId = $genreId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }
}
