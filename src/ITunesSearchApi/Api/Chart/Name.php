<?php
namespace ITunesSearchApi\Api\Chart;

use MyCLabs\Enum\Enum;

class Name extends Enum
{
    const PODCASTS = 'Podcasts';
    const AUDIO_PODCASTS = 'AudioPodcasts';
    const VIDEO_PODCASTS = 'VideoPodcasts';
    const PODCAST_EPISODES = 'PodcastEpisodes';
    const AUDIO_PODCAST_EPISODES = 'AudioPodcastEpisodes';
    const VIDEO_PODCAST_EPISODES = 'VideoPodcastEpisodes';

    // TODO add other names
}
