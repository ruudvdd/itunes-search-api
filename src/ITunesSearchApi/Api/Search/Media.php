<?php
namespace ITunesSearchApi\Api\Search;

use MyCLabs\Enum\Enum;

class Media extends Enum
{
    const MOVIE = 'movie';
    const PODCAST = 'podcast';
    const MUSIC = 'music';
    const MUSIC_VIDEO = 'musicVideo';
    const AUDIOBOOK = 'audiobook';
    const SHORT_FILM = 'shortFilm';
    const TV_SHOW = 'tvShow';
    const SOFTWARE = 'software';
    const EBOOK = 'ebook';
    const ALL = 'all';
}
