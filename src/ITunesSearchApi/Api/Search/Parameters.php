<?php
namespace ITunesSearchApi\Api\Search;

class Parameters
{
    /**
     * @var string
     */
    private $term;

    /**
     * @var string
     */
    private $country = 'US';

    /**
     * @var Entity|null
     */
    protected $entity = null;

    /**
     * @var int
     */
    protected $limit = 50;

    /**
     * @var string
     */
    protected $lang = 'en_us';

    /**
     * @var bool
     */
    protected $explicit = true;

    /**
     * @param string $term
     * @param string $country
     */
    public function __construct($term, $country = 'US')
    {
        $this->term = $term;
        $this->country = $country;
    }

    /**
     * @param string|Entity $entity
     * @return $this
     */
    public function setEntity($entity)
    {
        if (!($entity instanceof Entity)) {
            $entity = new Entity($entity);
        }
        $this->entity = $entity;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity->getValue();
    }

    /**
     * @return string
     */
    public function getMedia()
    {
        if ($this->entity === null) {
            return null;
        }

        return $this->entity->getMedia();
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param string $term
     * @return $this
     */
    public function setTerm($term)
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return $this
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isExplicit()
    {
        return $this->explicit;
    }

    /**
     * @return boolean
     */
    public function getExplicit()
    {
        return $this->explicit ? 'Yes' : 'No';
    }

    /**
     * @param boolean $explicit
     * @return $this
     */
    public function setExplicit($explicit)
    {
        $this->explicit = !!$explicit;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }
}
