<?php
namespace ITunesSearchApi\Api\Search;

use MyCLabs\Enum\Enum;

class Entity extends Enum
{
    const MEDIA_MOVIE_MOVIE_ARTIST = 'movieArtist';
    const MEDIA_MOVIE_MOVIE = 'movie';
    const MEDIA_PODCAST_PODCAST_AUTHOR = 'podcastAuthor';
    const MEDIA_PODCAST_PODCAST = 'podcast';
    const MEDIA_MUSIC_MUSIC_ARTIST = 'musicArtist';
    const MEDIA_MUSIC_MUSIC_TRACK = 'musicTrack';
    const MEDIA_MUSIC_ALBUM = 'album';
    const MEDIA_MUSIC_MUSIC_VIDEO = 'musicVideo';
    const MEDIA_MUSIC_MIX = 'mix';
    const MEDIA_MUSIC_SONG = 'song';
    const MEDIA_MUSIC_VIDEO_MUSIC_ARTIST = 'musicArtist';
    const MEDIA_MUSIC_VIDEO_MUSIC_VIDEO = 'musicVideo';
    const MEDIA_AUDIOBOOK_AUDIOBOOK_AUTHOR = 'audiobookAuthor';
    const MEDIA_AUDIOBOOK_AUDIOBOOK = 'audiobook';
    const MEDIA_SHORT_FILM_SHORT_FILM_ARTIST = 'shortFilmArtist';
    const MEDIA_SHORT_FILM_SHORT_FILM = 'shortFilm';
    const MEDIA_TV_SHOW_TV_EPISODE = 'tvEpisode';
    const MEDIA_TV_SHOW_TV_SEASON = 'tvSeason';
    const MEDIA_SOFTWARE_SOFTWARE = 'software';
    const MEDIA_SOFTWARE_IPAD_SOFTWARE = 'iPadSoftware';
    const MEDIA_SOFTWARE_MAC_SOFTWARE = 'macSoftware';
    const MEDIA_EBOOK_EBOOK = 'ebook';
    const MEDIA_ALL_MOVIE = 'movie';
    const MEDIA_ALL_ALBUM = 'album';
    const MEDIA_ALL_ALL_ARTIST = 'allArtist';
    const MEDIA_ALL_PODCAST = 'podcast';
    const MEDIA_ALL_MUSIC_VIDEO = 'musicVideo';
    const MEDIA_ALL_MIX = 'mix';
    const MEDIA_ALL_AUDIOBOOK = 'audiobook';
    const MEDIA_ALL_TV_SEASON = 'tvSeason';
    const MEDIA_ALL_ALL_TRACK = 'allTrack';

    /**
     * @var array
     */
    protected static $library = [];

    /**
     * @param string $value
     */
    public function __construct($value)
    {
        self::initLibrary();
        parent::__construct($value);
    }

    /**
     * @return string
     */
    public function getMedia()
    {
        if (!isset(self::$library[$this->value])) {
            throw new \LogicException('Can\'t find the representative media of entity \"' . $this->value . '\"');
        }
        return self::$library[$this->value];
    }

    private static function initLibrary()
    {
        self::$library = [
            self::MEDIA_MOVIE_MOVIE_ARTIST => Media::MOVIE,
            self::MEDIA_MOVIE_MOVIE => Media::MOVIE,
            self::MEDIA_PODCAST_PODCAST_AUTHOR => Media::PODCAST,
            self::MEDIA_PODCAST_PODCAST => Media::PODCAST,
            self::MEDIA_MUSIC_MUSIC_ARTIST => Media::MUSIC,
            self::MEDIA_MUSIC_MUSIC_TRACK => Media::MUSIC,
            self::MEDIA_MUSIC_ALBUM => Media::MUSIC,
            self::MEDIA_MUSIC_MUSIC_VIDEO => Media::MUSIC,
            self::MEDIA_MUSIC_MIX => Media::MUSIC,
            self::MEDIA_MUSIC_SONG => Media::MUSIC,
            self::MEDIA_MUSIC_VIDEO_MUSIC_ARTIST => Media::MUSIC_VIDEO,
            self::MEDIA_MUSIC_VIDEO_MUSIC_VIDEO => Media::MUSIC_VIDEO,
            self::MEDIA_AUDIOBOOK_AUDIOBOOK_AUTHOR => Media::AUDIOBOOK,
            self::MEDIA_AUDIOBOOK_AUDIOBOOK => Media::AUDIOBOOK,
            self::MEDIA_SHORT_FILM_SHORT_FILM_ARTIST => Media::SHORT_FILM,
            self::MEDIA_SHORT_FILM_SHORT_FILM => Media::SHORT_FILM,
            self::MEDIA_TV_SHOW_TV_EPISODE => Media::TV_SHOW,
            self::MEDIA_TV_SHOW_TV_SEASON => Media::TV_SHOW,
            self::MEDIA_SOFTWARE_SOFTWARE => Media::SOFTWARE,
            self::MEDIA_SOFTWARE_IPAD_SOFTWARE => Media::SOFTWARE,
            self::MEDIA_SOFTWARE_MAC_SOFTWARE => Media::SOFTWARE,
            self::MEDIA_EBOOK_EBOOK => Media::EBOOK,
            self::MEDIA_ALL_MOVIE => Media::ALL,
            self::MEDIA_ALL_ALBUM => Media::ALL,
            self::MEDIA_ALL_ALL_ARTIST => Media::ALL,
            self::MEDIA_ALL_PODCAST => Media::ALL,
            self::MEDIA_ALL_MUSIC_VIDEO => Media::ALL,
            self::MEDIA_ALL_MIX => Media::ALL,
            self::MEDIA_ALL_AUDIOBOOK => Media::ALL,
            self::MEDIA_ALL_TV_SEASON => Media::ALL,
            self::MEDIA_ALL_ALL_TRACK => Media::ALL
        ];
    }
}
