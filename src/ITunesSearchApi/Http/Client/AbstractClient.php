<?php
namespace ITunesSearchApi\Http\Client;

use ITunesSearchApi\Api\Search\Parameters as SearchParameters;
use ITunesSearchApi\Api\Lookup\Parameters as LookupParameters;
use ITunesSearchApi\Api\Chart\Parameters as ChartParameters;
use ITunesSearchApi\Exception\RequestFailed;
use ITunesSearchApi\Http\Search\ParameterQueryBuilder as SearchParameterQueryBuilder;
use ITunesSearchApi\Http\Lookup\ParameterQueryBuilder as LookupParameterQueryBuilder;
use ITunesSearchApi\Http\Chart\ParameterQueryBuilder as ChartParameterQueryBuilder;

abstract class AbstractClient
{
    /**
     * @var string
     */
    protected $host = 'https://itunes.apple.com';

    /**
     * @var string
     */
    protected $searchEndpoint = 'search';

    /**
     * @var string
     */
    protected $lookupEndpoint = 'lookup';

    /**
     * @var string
     */
    protected $genresEndpoint = 'WebObjects/MZStoreServices.woa/ws/genres';

    /**
     * @var string
     */
    protected $chartEndpoint = 'WebObjects/MZStoreServices.woa/ws/charts';

    /**
     * @var array
     */
    protected $config;

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function search(SearchParameters $request)
    {
        $url = $this->getSearchUrl($request);
        $response = $this->get($url);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function lookup(LookupParameters $request)
    {
        $url = $this->getLookupUrl($request);
        $response = $this->get($url);
        return $response;
    }

    /**
     * @inheritdoc
     */
    public function genres($genreId = null)
    {
        $url = $this->host . '/' . $this->genresEndpoint;
        if (!empty($genreId)) {
            $url .= '?' . 'id=' . $genreId;
        }
        return $this->get($url);
    }

    public function chart(ChartParameters $request)
    {
        $url = $this->getChartUrl($request);
        $response = $this->get($url);
        return $response;
    }

    /**
     * @param SearchParameters $request
     * @return string
     */
    protected function getSearchUrl(SearchParameters $request)
    {
        return $this->host . '/' . $this->searchEndpoint . '?' . $this->buildSearchQueryString($request);
    }

    /**
     * @return SearchParameterQueryBuilder
     */
    protected function getSearchParameterQueryBuilder()
    {
        $config = $this->config;
        $configEntry = 'search_parameter_query_builder';

        $parameterQueryBuilder =
            !isset($config[$configEntry]) || !($config[$configEntry] instanceof SearchParameterQueryBuilder) ?
                new SearchParameterQueryBuilder() :
                $config[$configEntry];

        return $parameterQueryBuilder;
    }

    /**
     * @param SearchParameters $parameters
     * @return string
     */
    protected function buildSearchQueryString(SearchParameters $parameters)
    {
        return $this->getSearchParameterQueryBuilder()->build($parameters);
    }

    /**
     * @param LookupParameters $request
     * @return string
     */
    protected function getLookupUrl(LookupParameters $request)
    {
        return $this->host . '/' . $this->lookupEndpoint . '?' . $this->buildLookupQueryString($request);
    }

    /**
     * @return LookupParameterQueryBuilder
     */
    protected function getLookupParameterQueryBuilder()
    {
        $config = $this->config;
        $configEntry = 'lookup_parameter_query_builder';
        $parameterQueryBuilder =
            !isset($config[$configEntry]) || !($config[$configEntry] instanceof LookupParameterQueryBuilder) ?
                new LookupParameterQueryBuilder() :
                $config[$configEntry];
        return $parameterQueryBuilder;
    }

    /**
     * @param LookupParameters $parameters
     * @return string
     */
    protected function buildLookupQueryString(LookupParameters $parameters)
    {
        return $this->getLookupParameterQueryBuilder()->build($parameters);
    }

    /**
     * @param ChartParameters $parameters
     * @return string
     */
    private function getChartUrl(ChartParameters $parameters)
    {
        return $this->host . '/' . $this->chartEndpoint . '?' . $this->buildChartQueryString($parameters);
    }

    /**
     * @return ChartParameterQueryBuilder
     */
    protected function getChartParameterQueryBuilder()
    {
        $config = $this->config;
        $configEntry = 'chart_parameter_query_builder';
        $parameterQueryBuilder =
            !isset($config[$configEntry]) || !($config[$configEntry] instanceof ChartParameterQueryBuilder) ?
                new ChartParameterQueryBuilder() :
                $config[$configEntry];
        return $parameterQueryBuilder;
    }

    /**
     * @param ChartParameters $parameters
     * @return string
     */
    protected function buildChartQueryString(ChartParameters $parameters)
    {
        return $this->getChartParameterQueryBuilder()->build($parameters);
    }

    /**
     * @param int $httpStatusCode
     * @param string $body
     * @throws RequestFailed
     * @return string
     */
    protected function handleResponse($httpStatusCode, $body)
    {
        if ($httpStatusCode !== 200) {
            $this->requestFailed($httpStatusCode, $body);
        }
        return json_decode($body, true);
    }

    /**
     * @param int $httpStatusCode
     * @param string $body
     */
    protected function requestFailed($httpStatusCode, $body)
    {
        throw new RequestFailed($body, $httpStatusCode);
    }

    /**
     * @param $url
     * @throws RequestFailed
     * @return array
     */
    abstract protected function get($url);
}
