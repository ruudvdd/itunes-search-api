<?php
namespace ITunesSearchApi\Http\Client;

use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use ITunesSearchApi\Http\ClientInterface;

class Guzzle extends AbstractClient implements ClientInterface
{
    /**
     * @var GuzzleClientInterface
     */
    private $client;

    /**
     * @param GuzzleClientInterface $client
     * @param array $config
     */
    public function __construct(GuzzleClientInterface $client, array $config = [])
    {
        $this->client = $client;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function get($url)
    {
        $response = $this->client->request('get', $url);
        return $this->handleResponse($response->getStatusCode(), (string) $response->getBody());
    }
}
