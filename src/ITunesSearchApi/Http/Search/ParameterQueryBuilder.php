<?php
namespace ITunesSearchApi\Http\Search;

use ITunesSearchApi\Api\Search\Parameters;

class ParameterQueryBuilder
{
    /**
     * @param Parameters $parameters
     * @return string
     */
    public function build(Parameters $parameters)
    {
        $queryAssocArray = array_filter([
            'term' => $parameters->getTerm(),
            'country' => $parameters->getCountry(),
            'media' => $parameters->getMedia(),
            'entity' => $parameters->getEntity(),
            'limit' => $parameters->getLimit(),
            'lang' => $parameters->getLang(),
            'explicit' => $parameters->getExplicit()
        ]);

        $queryArray = array_map(function ($key, $value) {
            return urlencode($key) . '=' . urlencode($value);
        }, array_keys($queryAssocArray), $queryAssocArray);

        return implode('&', $queryArray);
    }
}
