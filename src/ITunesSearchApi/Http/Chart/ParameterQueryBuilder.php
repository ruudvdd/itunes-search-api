<?php
namespace ITunesSearchApi\Http\Chart;

use ITunesSearchApi\Api\Chart\Parameters;

class ParameterQueryBuilder
{
    /**
     * @param Parameters $parameters
     * @return string
     */
    public function build(Parameters $parameters)
    {
        $queryAssocArray = array_filter([
            'name' => $parameters->getName(),
            'cc' => $parameters->getCountry(),
            'g' => $parameters->getGenreId(),
            'limit' => $parameters->getLimit()
        ]);

        $queryArray = array_map(function ($key, $value) {
            return urlencode($key) . '=' . urlencode($value);
        }, array_keys($queryAssocArray), $queryAssocArray);

        return implode('&', $queryArray);
    }
}
