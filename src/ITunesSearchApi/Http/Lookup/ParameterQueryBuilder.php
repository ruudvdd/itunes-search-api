<?php
namespace ITunesSearchApi\Http\Lookup;

use ITunesSearchApi\Api\Lookup\Parameters;

class ParameterQueryBuilder
{
    /**
     * @param Parameters $parameters
     * @return string
     */
    public function build(Parameters $parameters)
    {
        $queryAssocArray = array_filter([
            $parameters->getIdName() => $parameters->getIdValue(),
            'entity' => $parameters->getEntity(),
            'sort' => $parameters->getSort(),
            'limit' => $parameters->getLimit()
        ]);

        $queryArray = array_map(function ($key, $value) {
            return urlencode($key) . '=' . urlencode($value);
        }, array_keys($queryAssocArray), $queryAssocArray);

        return implode('&', $queryArray);
    }
}
