<?php
namespace ITunesSearchApi\Http;

use ITunesSearchApi\Api\Search\Parameters as SearchParameters;
use ITunesSearchApi\Api\Lookup\Parameters as LookupParameters;

interface ClientInterface
{
    /**
     * @see https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
     * use the search api of iTunes
     *
     * @param SearchParameters $request
     * @return array
     */
    public function search(SearchParameters $request);

    /**
     * @see https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
     * use the search api of iTunes
     *
     * @param LookupParameters $request
     * @return array
     */
    public function lookup(LookupParameters $request);

    /**
     * @see https://affiliate.itunes.apple.com/resources/documentation/genre-mapping/
     * get all genres or a specific genre with their subgenres
     *
     * @param int|null $genreId leave this parameter empty when you want all genres with their details,
     * or give a specific genre id to get it genre and its details
     * @return mixed
     */
    public function genres($genreId = null);
}
